﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordMixer
{
   public class Mixer
    {
       /// <summary>
        /// Перемешивает буквы в словах, оставляя нетронутыми первыую и последнюю буквы
       /// </summary>
       /// <param name="original">Текст</param>
       /// <returns>Сконвертированный текст</returns>
       public static string Mix(string original)
       {
           var words = GetWordsFromText(original);
           StringBuilder outText = new StringBuilder();
           foreach (var word in words)
           {
               outText.Append(ConvertWord(word) + " ");
           }
           return outText.ToString();
       }

       /// <summary>
       /// Извлекает из текста отдельные слова
       /// </summary>
       /// <param name="input"></param>
       /// <returns>Массив слов</returns>
       private static string[] GetWordsFromText(string input)
       {
           var preWords = input.Split(new[] { '-', '.', '?', '!', ')', '(', ',', ':' }, StringSplitOptions.RemoveEmptyEntries);
           var words = new List<string>();
           foreach (var word in preWords)
           {
               words.AddRange(word.Split());
           }
           return words.ToArray();
       }

       /// <summary>
       /// Перемешивает буквы в слове, не трогая первое и последнее
       /// </summary>
       /// <param name="input"></param>
       /// <returns>Перемшанное слово</returns>
       private static string ConvertWord(string input)
       {
           int lenght = input.Length;
           if (lenght > 3)
           {
               string first = input.Substring(0, 1);
               string last = input.Substring(lenght - 1, 1);
               string middle = input.Substring(1, lenght - 2);
               string result = first;
               List<char> list = new List<char>(middle.ToCharArray());
               Random r = new Random();
               int i;
               while (list.Count > 1)
               {
                   i = r.Next(list.Count);
                   result += list[i];
                   list.RemoveAt(i);
               }
               result += list[0] + last;
               return result;
           }
           return input;
       }


    }
}
